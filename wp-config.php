<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'u6926690_marusindo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9]w>AMDNh6&G!by=@~DkVx#JTM,YFwpDz?8POHnW@4O~X0`yd.kX42}Ep!P)*nOx' );
define( 'SECURE_AUTH_KEY',  'or49,ZTpNn:9[aKXJ{&7bUcV1e(|7%rSR|4*Al-RJ;qp`_<}$g4.=8#BnK,T_7h[' );
define( 'LOGGED_IN_KEY',    '-R1PErX4994*f[&Zi P~l3q-$_OVI|nnNm} n?Vr&skknp/Dd:m`Gxn$]cCA^u`6' );
define( 'NONCE_KEY',        '>FU/H0rE$<dqOSk%&lGMk}{fllwy(zvaP toP7<9.zD7E{h4C#g#&05IVG NpZ`~' );
define( 'AUTH_SALT',        '||YbV=7JtUrKPL[t4=>BPv~1sXz:+[Q>F[ (y^SgEu-ln9c@=A)$,SZYh?9qO6R$' );
define( 'SECURE_AUTH_SALT', 'HE[dQ5`7h5ehDdK{YY%P^nI,VL/RuiZggUG1_0tuB+m;I{#v08ctAG]JY<%qD[rf' );
define( 'LOGGED_IN_SALT',   '3y2gD,Gu/;O L$C0V]{?B@Iz|Zbjv84<n4:|[z/s+J5{9[)kBHX2s_X*G@Q7^xIP' );
define( 'NONCE_SALT',       'Bt@UuJgV} /<uX2ZR2wwzx!OFRZy,I}-gor}`(k|9Bhz2jIoZqlxJo>,-_,~Shq+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
