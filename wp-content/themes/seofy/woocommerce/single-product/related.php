<?php
/**
 * Related Products
 *
 * This template is overridden by WebGeniusLab team.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$show_related = !empty(Seofy_Theme_Helper::get_option('shop_single_related')) ? true : (bool) Seofy_Theme_Helper::get_option('shop_single_related');
$columns = (int) Seofy_Theme_Helper::get_option('shop_related_columns');
$count = (int) Seofy_Theme_Helper::get_option('shop_r_products_per_page');        

if ( $related_products && $show_related ) : ?>

<section class="related products">

	<?php
	$heading = apply_filters( 'seofy_related_products_heading', __( 'Related products', 'seofy' ) );

	if ( $heading ) :
		?>
		<h2><?php echo esc_html( $heading ); ?></h2>
	<?php endif; ?>
	<div class="wgl-products-related wgl-products-wrapper columns-<?php echo esc_attr($columns);?>">
		<?php woocommerce_product_loop_start(); ?>
		
		<?php foreach ( $related_products as $related_product ) : ?>

			<?php
			$post_object = get_post( $related_product->get_id() );

			setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

			wc_get_template_part( 'content', 'product' );
			?>

		<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>
	</div>
</section>
	<?php
endif;

wp_reset_postdata();
